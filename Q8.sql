SELECT 
item_id,
item_name,
item_price,
category_name

FROM
item t1
INNER JOIN
item_category t2
ON
t1.category_id = t2.category_id
