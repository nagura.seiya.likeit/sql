SELECT
category_name,
SUM(t1.item_price) AS total_price


FROM
item t1

INNER JOIN
item_category t2

ON
t1.category_id = t2.category_id

GROUP BY
t1.category_id

ORDER BY
total_price DESC

